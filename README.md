# URSS-Ajedrez

Esta aplicación funciona como un tablero de ajedrez para dos jugadores; está diseñado para que ambos jugadores tengas plena libertad a la hora de desarrollar sus jugadas de una manera más cómoda.

## Contenidos:

- [Como funciona?](#mi-titulo-a-anclar)
- [Colocación de las piezas en el tablero](#mi-titulo-a-anclar)
- [Movimiento de las piezas](#mi-titulo-a-anclar)
- [Organización de la pantalla de inicio](###Como-funciona?)
- [Reglas del juego](###Reglas-del-juego)


### Como funciona?

Al comenzar aparecerá un menú donde te pedirá el nombre tanto del jugador1 como del jugador2. Una vez introducido, presiona comenzar para que aparezca el tablero con sus piezas colocadas.
Dentro del tablero simplemente hay que presionar la pieza que se quiera mover y luego la zona de destino.  Cuando se quiera eliminar una pieza simplemente hay que pulsar la pieza deseada y luego pulsar en el cementerio para dejar la pieza.
Si uno de los jugadores se rinde solo tiene que pulsar el botón de rendirse y terminará la partida.

### Colocación de las piezas en el tablero:

Para ordenar las piezas en las casillas solo hay que crear dos arrays que recojan las diferentes partes.
```java
Dim casillas() As PictureBox
Dim piezas() As PictureBox
piezas = New PictureBox() {torreN1, caballoN1, arfilN1, reinaN, reyN, arfilN2, caballoN2, torreN2, peonN1, peonN2, peonN3, peonN4, peonN5, peonN6, peonN7, peonN8, peonB1, peonB2, peonB3, peonB4, peonB5, peonB6, peonB7, peonB8, torreB1, caballoB1, arfilB1, reinaB, reyB, arfilB2, caballoB2, torreB2}
casillas = New PictureBox() {A8, B8, C8, D8, E8, F8, G8, H8, A7, B7, C7, D7, E7, F7, G7, H7, A6, B6, C6, D6, E6, F6, G6, H6, A5, B5, C5, D5, E5, F5, G5, H5, A4, B4, C4, D4, E4, F4, G4, H4, A3, B3, C3, D3, E3, F3, G3, H3, A2, B2, C2, D2, E2, F2, G2, H2, A1, B1, C1, D1, E1, F1, G1, H1}
```
Luego, mediante una función, se ordena todo automáticamente.
```java
Private Function colocacion()
		i = 16
		For j = 0 To 63
			If j <= 15 Then
				piezas(j).Parent = casillas(j)
				piezas(j).BringToFront()
			End If
			If j >= 48 Then
				piezas(i).Parent = casillas(j)
				piezas(i).BringToFront()
				i = i + 1
			End If
			casillas(j).Parent = tablero
		Next j
	End Function
```

### Movimiento de las piezas:

Para realizar los movimientos de las piezas se usan dos funciones por cada pieza, una se encarga de agarrar la pieza y la otra de colocarla.
```java
Private Sub torreN1_MouseClick(sender As Object, e As MouseEventArgs) Handles torreN1.MouseClick
		pulsada = 0
	End Sub
Private Sub A1_Click(sender As Object, e As EventArgs) Handles A1.Click
		piezas(pulsada).Parent = A1
	End Sub
```

### Organización de la pantalla de inicio:

Primero se ocultan los primeros objetos creaos al fondo, para que en pantalla solo aparezcan los objetos deseados.
Para los nuevos objetos, estos son introducidos en un array (menos los que no pueden ser introducidos) y mediante una función se esconden y pasan su información en su momento.
```java
inicio = New Label() {titulo, TextJugador1, TextJugador2}
Private Sub comenzar_Click(sender As Object, e As EventArgs) Handles comenzar.Click
		PantallaInicio.SendToBack()
		jugador1.Text = IntrJugador1.Text
		jugador2.Text = IntrJugador2.Text
	End Sub
```
### Reglas del juego:

En [esta página](https://thezugzwangblog.com/reglas-del-ajedrez/) podrás encontrar las reglas para jugar al ajedrez de manera correcta. 

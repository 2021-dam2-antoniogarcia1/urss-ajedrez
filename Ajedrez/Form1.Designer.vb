﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class mesa
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(mesa))
        Me.rendirseN = New System.Windows.Forms.Button()
        Me.rendirseB = New System.Windows.Forms.Button()
        Me.jugador1 = New System.Windows.Forms.Label()
        Me.jugador2 = New System.Windows.Forms.Label()
        Me.torreB2 = New System.Windows.Forms.PictureBox()
        Me.caballoB2 = New System.Windows.Forms.PictureBox()
        Me.arfilB2 = New System.Windows.Forms.PictureBox()
        Me.reyB = New System.Windows.Forms.PictureBox()
        Me.reinaB = New System.Windows.Forms.PictureBox()
        Me.arfilB1 = New System.Windows.Forms.PictureBox()
        Me.caballoB1 = New System.Windows.Forms.PictureBox()
        Me.torreB1 = New System.Windows.Forms.PictureBox()
        Me.peonB8 = New System.Windows.Forms.PictureBox()
        Me.peonB7 = New System.Windows.Forms.PictureBox()
        Me.peonB6 = New System.Windows.Forms.PictureBox()
        Me.peonB5 = New System.Windows.Forms.PictureBox()
        Me.peonB4 = New System.Windows.Forms.PictureBox()
        Me.peonB3 = New System.Windows.Forms.PictureBox()
        Me.peonB2 = New System.Windows.Forms.PictureBox()
        Me.peonB1 = New System.Windows.Forms.PictureBox()
        Me.peonN8 = New System.Windows.Forms.PictureBox()
        Me.peonN7 = New System.Windows.Forms.PictureBox()
        Me.peonN6 = New System.Windows.Forms.PictureBox()
        Me.peonN5 = New System.Windows.Forms.PictureBox()
        Me.peonN4 = New System.Windows.Forms.PictureBox()
        Me.peonN3 = New System.Windows.Forms.PictureBox()
        Me.peonN2 = New System.Windows.Forms.PictureBox()
        Me.peonN1 = New System.Windows.Forms.PictureBox()
        Me.torreN2 = New System.Windows.Forms.PictureBox()
        Me.caballoN2 = New System.Windows.Forms.PictureBox()
        Me.arfilN2 = New System.Windows.Forms.PictureBox()
        Me.reyN = New System.Windows.Forms.PictureBox()
        Me.reinaN = New System.Windows.Forms.PictureBox()
        Me.arfilN1 = New System.Windows.Forms.PictureBox()
        Me.caballoN1 = New System.Windows.Forms.PictureBox()
        Me.torreN1 = New System.Windows.Forms.PictureBox()
        Me.auxiliar = New System.Windows.Forms.PictureBox()
        Me.H3 = New System.Windows.Forms.PictureBox()
        Me.G3 = New System.Windows.Forms.PictureBox()
        Me.F3 = New System.Windows.Forms.PictureBox()
        Me.E3 = New System.Windows.Forms.PictureBox()
        Me.D3 = New System.Windows.Forms.PictureBox()
        Me.C3 = New System.Windows.Forms.PictureBox()
        Me.B3 = New System.Windows.Forms.PictureBox()
        Me.A3 = New System.Windows.Forms.PictureBox()
        Me.A4 = New System.Windows.Forms.PictureBox()
        Me.D4 = New System.Windows.Forms.PictureBox()
        Me.E4 = New System.Windows.Forms.PictureBox()
        Me.F4 = New System.Windows.Forms.PictureBox()
        Me.G4 = New System.Windows.Forms.PictureBox()
        Me.H4 = New System.Windows.Forms.PictureBox()
        Me.C4 = New System.Windows.Forms.PictureBox()
        Me.B4 = New System.Windows.Forms.PictureBox()
        Me.A5 = New System.Windows.Forms.PictureBox()
        Me.B5 = New System.Windows.Forms.PictureBox()
        Me.H5 = New System.Windows.Forms.PictureBox()
        Me.G5 = New System.Windows.Forms.PictureBox()
        Me.F5 = New System.Windows.Forms.PictureBox()
        Me.E5 = New System.Windows.Forms.PictureBox()
        Me.D5 = New System.Windows.Forms.PictureBox()
        Me.C5 = New System.Windows.Forms.PictureBox()
        Me.H6 = New System.Windows.Forms.PictureBox()
        Me.G6 = New System.Windows.Forms.PictureBox()
        Me.F6 = New System.Windows.Forms.PictureBox()
        Me.E6 = New System.Windows.Forms.PictureBox()
        Me.D6 = New System.Windows.Forms.PictureBox()
        Me.C6 = New System.Windows.Forms.PictureBox()
        Me.B6 = New System.Windows.Forms.PictureBox()
        Me.A6 = New System.Windows.Forms.PictureBox()
        Me.fotoJ2 = New System.Windows.Forms.PictureBox()
        Me.fotoJ1 = New System.Windows.Forms.PictureBox()
        Me.H1 = New System.Windows.Forms.PictureBox()
        Me.H2 = New System.Windows.Forms.PictureBox()
        Me.G2 = New System.Windows.Forms.PictureBox()
        Me.E2 = New System.Windows.Forms.PictureBox()
        Me.D2 = New System.Windows.Forms.PictureBox()
        Me.F2 = New System.Windows.Forms.PictureBox()
        Me.C2 = New System.Windows.Forms.PictureBox()
        Me.B2 = New System.Windows.Forms.PictureBox()
        Me.H7 = New System.Windows.Forms.PictureBox()
        Me.G7 = New System.Windows.Forms.PictureBox()
        Me.F7 = New System.Windows.Forms.PictureBox()
        Me.E7 = New System.Windows.Forms.PictureBox()
        Me.D7 = New System.Windows.Forms.PictureBox()
        Me.C7 = New System.Windows.Forms.PictureBox()
        Me.B7 = New System.Windows.Forms.PictureBox()
        Me.G8 = New System.Windows.Forms.PictureBox()
        Me.F8 = New System.Windows.Forms.PictureBox()
        Me.G1 = New System.Windows.Forms.PictureBox()
        Me.F1 = New System.Windows.Forms.PictureBox()
        Me.E1 = New System.Windows.Forms.PictureBox()
        Me.D1 = New System.Windows.Forms.PictureBox()
        Me.C1 = New System.Windows.Forms.PictureBox()
        Me.A2 = New System.Windows.Forms.PictureBox()
        Me.B1 = New System.Windows.Forms.PictureBox()
        Me.A7 = New System.Windows.Forms.PictureBox()
        Me.A1 = New System.Windows.Forms.PictureBox()
        Me.H8 = New System.Windows.Forms.PictureBox()
        Me.E8 = New System.Windows.Forms.PictureBox()
        Me.D8 = New System.Windows.Forms.PictureBox()
        Me.C8 = New System.Windows.Forms.PictureBox()
        Me.B8 = New System.Windows.Forms.PictureBox()
        Me.A8 = New System.Windows.Forms.PictureBox()
        Me.tablero = New System.Windows.Forms.PictureBox()
        Me.cementerio = New System.Windows.Forms.PictureBox()
        Me.cementeriTexto = New System.Windows.Forms.Label()
        Me.titulo = New System.Windows.Forms.Label()
        Me.comenzar = New System.Windows.Forms.Button()
        Me.PantallaInicio = New System.Windows.Forms.PictureBox()
        Me.IntrJugador1 = New System.Windows.Forms.TextBox()
        Me.TextJugador1 = New System.Windows.Forms.Label()
        Me.TextJugador2 = New System.Windows.Forms.Label()
        Me.IntrJugador2 = New System.Windows.Forms.TextBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        CType(Me.torreB2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.caballoB2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.arfilB2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.reyB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.reinaB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.arfilB1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.caballoB1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.torreB1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.peonB8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.peonB7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.peonB6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.peonB5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.peonB4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.peonB3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.peonB2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.peonB1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.peonN8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.peonN7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.peonN6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.peonN5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.peonN4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.peonN3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.peonN2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.peonN1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.torreN2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.caballoN2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.arfilN2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.reyN, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.reinaN, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.arfilN1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.caballoN1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.torreN1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.auxiliar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.H3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.G3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.F3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.E3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.D3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.B3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.A3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.A4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.D4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.E4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.F4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.G4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.H4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.B4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.A5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.B5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.H5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.G5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.F5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.E5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.D5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.H6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.G6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.F6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.E6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.D6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.B6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.A6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.fotoJ2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.fotoJ1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.H1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.H2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.G2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.E2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.D2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.F2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.B2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.H7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.G7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.F7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.E7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.D7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.B7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.G8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.F8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.G1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.F1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.E1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.D1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.A2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.B1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.A7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.A1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.H8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.E8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.D8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.B8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.A8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tablero, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cementerio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PantallaInicio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'rendirseN
        '
        Me.rendirseN.Location = New System.Drawing.Point(443, 120)
        Me.rendirseN.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.rendirseN.Name = "rendirseN"
        Me.rendirseN.Size = New System.Drawing.Size(147, 38)
        Me.rendirseN.TabIndex = 19
        Me.rendirseN.Text = "Rendirse"
        Me.rendirseN.UseVisualStyleBackColor = True
        '
        'rendirseB
        '
        Me.rendirseB.Location = New System.Drawing.Point(443, 371)
        Me.rendirseB.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.rendirseB.Name = "rendirseB"
        Me.rendirseB.Size = New System.Drawing.Size(147, 38)
        Me.rendirseB.TabIndex = 20
        Me.rendirseB.Text = "Rendirse"
        Me.rendirseB.UseVisualStyleBackColor = True
        '
        'jugador1
        '
        Me.jugador1.AutoSize = True
        Me.jugador1.Location = New System.Drawing.Point(537, 16)
        Me.jugador1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.jugador1.Name = "jugador1"
        Me.jugador1.Size = New System.Drawing.Size(54, 13)
        Me.jugador1.TabIndex = 21
        Me.jugador1.Text = "Jugador 1"
        '
        'jugador2
        '
        Me.jugador2.AutoSize = True
        Me.jugador2.Location = New System.Drawing.Point(537, 266)
        Me.jugador2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.jugador2.Name = "jugador2"
        Me.jugador2.Size = New System.Drawing.Size(54, 13)
        Me.jugador2.TabIndex = 22
        Me.jugador2.Text = "Jugador 2"
        '
        'torreB2
        '
        Me.torreB2.BackColor = System.Drawing.Color.Transparent
        Me.torreB2.Image = Global.Ajedrez.My.Resources.Resources.torreBlanca
        Me.torreB2.Location = New System.Drawing.Point(0, 0)
        Me.torreB2.Margin = New System.Windows.Forms.Padding(0)
        Me.torreB2.Name = "torreB2"
        Me.torreB2.Size = New System.Drawing.Size(50, 50)
        Me.torreB2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.torreB2.TabIndex = 144
        Me.torreB2.TabStop = False
        '
        'caballoB2
        '
        Me.caballoB2.BackColor = System.Drawing.Color.Transparent
        Me.caballoB2.Image = Global.Ajedrez.My.Resources.Resources.caballoBlanco
        Me.caballoB2.Location = New System.Drawing.Point(0, 0)
        Me.caballoB2.Margin = New System.Windows.Forms.Padding(0)
        Me.caballoB2.Name = "caballoB2"
        Me.caballoB2.Size = New System.Drawing.Size(50, 50)
        Me.caballoB2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.caballoB2.TabIndex = 143
        Me.caballoB2.TabStop = False
        '
        'arfilB2
        '
        Me.arfilB2.BackColor = System.Drawing.Color.Transparent
        Me.arfilB2.Image = Global.Ajedrez.My.Resources.Resources.arfilBlanco
        Me.arfilB2.Location = New System.Drawing.Point(0, 0)
        Me.arfilB2.Margin = New System.Windows.Forms.Padding(0)
        Me.arfilB2.Name = "arfilB2"
        Me.arfilB2.Size = New System.Drawing.Size(50, 50)
        Me.arfilB2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.arfilB2.TabIndex = 142
        Me.arfilB2.TabStop = False
        '
        'reyB
        '
        Me.reyB.BackColor = System.Drawing.Color.Transparent
        Me.reyB.Image = Global.Ajedrez.My.Resources.Resources.Chess_klt451
        Me.reyB.Location = New System.Drawing.Point(0, 0)
        Me.reyB.Margin = New System.Windows.Forms.Padding(0)
        Me.reyB.Name = "reyB"
        Me.reyB.Size = New System.Drawing.Size(50, 50)
        Me.reyB.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.reyB.TabIndex = 141
        Me.reyB.TabStop = False
        '
        'reinaB
        '
        Me.reinaB.BackColor = System.Drawing.Color.Transparent
        Me.reinaB.Image = Global.Ajedrez.My.Resources.Resources.reinaBlanca
        Me.reinaB.Location = New System.Drawing.Point(0, 0)
        Me.reinaB.Margin = New System.Windows.Forms.Padding(0)
        Me.reinaB.Name = "reinaB"
        Me.reinaB.Size = New System.Drawing.Size(50, 50)
        Me.reinaB.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.reinaB.TabIndex = 140
        Me.reinaB.TabStop = False
        '
        'arfilB1
        '
        Me.arfilB1.BackColor = System.Drawing.Color.Transparent
        Me.arfilB1.Image = Global.Ajedrez.My.Resources.Resources.arfilBlanco
        Me.arfilB1.Location = New System.Drawing.Point(0, 0)
        Me.arfilB1.Margin = New System.Windows.Forms.Padding(0)
        Me.arfilB1.Name = "arfilB1"
        Me.arfilB1.Size = New System.Drawing.Size(50, 50)
        Me.arfilB1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.arfilB1.TabIndex = 139
        Me.arfilB1.TabStop = False
        '
        'caballoB1
        '
        Me.caballoB1.BackColor = System.Drawing.Color.Transparent
        Me.caballoB1.Image = Global.Ajedrez.My.Resources.Resources.caballoBlanco
        Me.caballoB1.Location = New System.Drawing.Point(0, 0)
        Me.caballoB1.Margin = New System.Windows.Forms.Padding(0)
        Me.caballoB1.Name = "caballoB1"
        Me.caballoB1.Size = New System.Drawing.Size(50, 50)
        Me.caballoB1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.caballoB1.TabIndex = 138
        Me.caballoB1.TabStop = False
        '
        'torreB1
        '
        Me.torreB1.BackColor = System.Drawing.Color.Transparent
        Me.torreB1.Image = Global.Ajedrez.My.Resources.Resources.torreBlanca
        Me.torreB1.Location = New System.Drawing.Point(0, 0)
        Me.torreB1.Margin = New System.Windows.Forms.Padding(0)
        Me.torreB1.Name = "torreB1"
        Me.torreB1.Size = New System.Drawing.Size(50, 50)
        Me.torreB1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.torreB1.TabIndex = 137
        Me.torreB1.TabStop = False
        '
        'peonB8
        '
        Me.peonB8.BackColor = System.Drawing.Color.Transparent
        Me.peonB8.Image = Global.Ajedrez.My.Resources.Resources.peonBlanco
        Me.peonB8.Location = New System.Drawing.Point(0, 0)
        Me.peonB8.Margin = New System.Windows.Forms.Padding(0)
        Me.peonB8.Name = "peonB8"
        Me.peonB8.Size = New System.Drawing.Size(50, 50)
        Me.peonB8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.peonB8.TabIndex = 136
        Me.peonB8.TabStop = False
        '
        'peonB7
        '
        Me.peonB7.BackColor = System.Drawing.Color.Transparent
        Me.peonB7.Image = Global.Ajedrez.My.Resources.Resources.peonBlanco
        Me.peonB7.Location = New System.Drawing.Point(0, 0)
        Me.peonB7.Margin = New System.Windows.Forms.Padding(0)
        Me.peonB7.Name = "peonB7"
        Me.peonB7.Size = New System.Drawing.Size(50, 50)
        Me.peonB7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.peonB7.TabIndex = 135
        Me.peonB7.TabStop = False
        '
        'peonB6
        '
        Me.peonB6.BackColor = System.Drawing.Color.Transparent
        Me.peonB6.Image = Global.Ajedrez.My.Resources.Resources.peonBlanco
        Me.peonB6.Location = New System.Drawing.Point(0, 0)
        Me.peonB6.Margin = New System.Windows.Forms.Padding(0)
        Me.peonB6.Name = "peonB6"
        Me.peonB6.Size = New System.Drawing.Size(50, 50)
        Me.peonB6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.peonB6.TabIndex = 134
        Me.peonB6.TabStop = False
        '
        'peonB5
        '
        Me.peonB5.BackColor = System.Drawing.Color.Transparent
        Me.peonB5.Image = Global.Ajedrez.My.Resources.Resources.peonBlanco
        Me.peonB5.Location = New System.Drawing.Point(0, 0)
        Me.peonB5.Margin = New System.Windows.Forms.Padding(0)
        Me.peonB5.Name = "peonB5"
        Me.peonB5.Size = New System.Drawing.Size(50, 50)
        Me.peonB5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.peonB5.TabIndex = 133
        Me.peonB5.TabStop = False
        '
        'peonB4
        '
        Me.peonB4.BackColor = System.Drawing.Color.Transparent
        Me.peonB4.Image = Global.Ajedrez.My.Resources.Resources.peonBlanco
        Me.peonB4.Location = New System.Drawing.Point(0, 0)
        Me.peonB4.Margin = New System.Windows.Forms.Padding(0)
        Me.peonB4.Name = "peonB4"
        Me.peonB4.Size = New System.Drawing.Size(50, 50)
        Me.peonB4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.peonB4.TabIndex = 132
        Me.peonB4.TabStop = False
        '
        'peonB3
        '
        Me.peonB3.BackColor = System.Drawing.Color.Transparent
        Me.peonB3.Image = Global.Ajedrez.My.Resources.Resources.peonBlanco
        Me.peonB3.Location = New System.Drawing.Point(0, 0)
        Me.peonB3.Margin = New System.Windows.Forms.Padding(0)
        Me.peonB3.Name = "peonB3"
        Me.peonB3.Size = New System.Drawing.Size(50, 50)
        Me.peonB3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.peonB3.TabIndex = 131
        Me.peonB3.TabStop = False
        '
        'peonB2
        '
        Me.peonB2.BackColor = System.Drawing.Color.Transparent
        Me.peonB2.Image = Global.Ajedrez.My.Resources.Resources.peonBlanco
        Me.peonB2.Location = New System.Drawing.Point(0, 0)
        Me.peonB2.Margin = New System.Windows.Forms.Padding(0)
        Me.peonB2.Name = "peonB2"
        Me.peonB2.Size = New System.Drawing.Size(50, 50)
        Me.peonB2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.peonB2.TabIndex = 130
        Me.peonB2.TabStop = False
        '
        'peonB1
        '
        Me.peonB1.BackColor = System.Drawing.Color.Transparent
        Me.peonB1.Image = Global.Ajedrez.My.Resources.Resources.peonBlanco
        Me.peonB1.Location = New System.Drawing.Point(0, 0)
        Me.peonB1.Margin = New System.Windows.Forms.Padding(0)
        Me.peonB1.Name = "peonB1"
        Me.peonB1.Size = New System.Drawing.Size(50, 50)
        Me.peonB1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.peonB1.TabIndex = 129
        Me.peonB1.TabStop = False
        '
        'peonN8
        '
        Me.peonN8.BackColor = System.Drawing.Color.Transparent
        Me.peonN8.Image = Global.Ajedrez.My.Resources.Resources.peonNegro
        Me.peonN8.Location = New System.Drawing.Point(0, 0)
        Me.peonN8.Margin = New System.Windows.Forms.Padding(0)
        Me.peonN8.Name = "peonN8"
        Me.peonN8.Size = New System.Drawing.Size(50, 50)
        Me.peonN8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.peonN8.TabIndex = 128
        Me.peonN8.TabStop = False
        '
        'peonN7
        '
        Me.peonN7.BackColor = System.Drawing.Color.Transparent
        Me.peonN7.Image = Global.Ajedrez.My.Resources.Resources.peonNegro
        Me.peonN7.Location = New System.Drawing.Point(0, 0)
        Me.peonN7.Margin = New System.Windows.Forms.Padding(0)
        Me.peonN7.Name = "peonN7"
        Me.peonN7.Size = New System.Drawing.Size(50, 50)
        Me.peonN7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.peonN7.TabIndex = 127
        Me.peonN7.TabStop = False
        '
        'peonN6
        '
        Me.peonN6.BackColor = System.Drawing.Color.Transparent
        Me.peonN6.Image = Global.Ajedrez.My.Resources.Resources.peonNegro
        Me.peonN6.Location = New System.Drawing.Point(0, 0)
        Me.peonN6.Margin = New System.Windows.Forms.Padding(0)
        Me.peonN6.Name = "peonN6"
        Me.peonN6.Size = New System.Drawing.Size(50, 50)
        Me.peonN6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.peonN6.TabIndex = 126
        Me.peonN6.TabStop = False
        '
        'peonN5
        '
        Me.peonN5.BackColor = System.Drawing.Color.Transparent
        Me.peonN5.Image = Global.Ajedrez.My.Resources.Resources.peonNegro
        Me.peonN5.Location = New System.Drawing.Point(0, 0)
        Me.peonN5.Margin = New System.Windows.Forms.Padding(0)
        Me.peonN5.Name = "peonN5"
        Me.peonN5.Size = New System.Drawing.Size(50, 50)
        Me.peonN5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.peonN5.TabIndex = 125
        Me.peonN5.TabStop = False
        '
        'peonN4
        '
        Me.peonN4.BackColor = System.Drawing.Color.Transparent
        Me.peonN4.Image = Global.Ajedrez.My.Resources.Resources.peonNegro
        Me.peonN4.Location = New System.Drawing.Point(0, 0)
        Me.peonN4.Margin = New System.Windows.Forms.Padding(0)
        Me.peonN4.Name = "peonN4"
        Me.peonN4.Size = New System.Drawing.Size(50, 50)
        Me.peonN4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.peonN4.TabIndex = 124
        Me.peonN4.TabStop = False
        '
        'peonN3
        '
        Me.peonN3.BackColor = System.Drawing.Color.Transparent
        Me.peonN3.Image = Global.Ajedrez.My.Resources.Resources.peonNegro
        Me.peonN3.Location = New System.Drawing.Point(0, 0)
        Me.peonN3.Margin = New System.Windows.Forms.Padding(0)
        Me.peonN3.Name = "peonN3"
        Me.peonN3.Size = New System.Drawing.Size(50, 50)
        Me.peonN3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.peonN3.TabIndex = 123
        Me.peonN3.TabStop = False
        '
        'peonN2
        '
        Me.peonN2.BackColor = System.Drawing.Color.Transparent
        Me.peonN2.Image = Global.Ajedrez.My.Resources.Resources.peonNegro
        Me.peonN2.Location = New System.Drawing.Point(0, 0)
        Me.peonN2.Margin = New System.Windows.Forms.Padding(0)
        Me.peonN2.Name = "peonN2"
        Me.peonN2.Size = New System.Drawing.Size(50, 50)
        Me.peonN2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.peonN2.TabIndex = 122
        Me.peonN2.TabStop = False
        '
        'peonN1
        '
        Me.peonN1.BackColor = System.Drawing.Color.Transparent
        Me.peonN1.Image = Global.Ajedrez.My.Resources.Resources.peonNegro
        Me.peonN1.Location = New System.Drawing.Point(0, 0)
        Me.peonN1.Margin = New System.Windows.Forms.Padding(0)
        Me.peonN1.Name = "peonN1"
        Me.peonN1.Size = New System.Drawing.Size(50, 50)
        Me.peonN1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.peonN1.TabIndex = 121
        Me.peonN1.TabStop = False
        '
        'torreN2
        '
        Me.torreN2.BackColor = System.Drawing.Color.Transparent
        Me.torreN2.Image = Global.Ajedrez.My.Resources.Resources.torreNegra
        Me.torreN2.Location = New System.Drawing.Point(0, 0)
        Me.torreN2.Margin = New System.Windows.Forms.Padding(0)
        Me.torreN2.Name = "torreN2"
        Me.torreN2.Size = New System.Drawing.Size(50, 50)
        Me.torreN2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.torreN2.TabIndex = 120
        Me.torreN2.TabStop = False
        '
        'caballoN2
        '
        Me.caballoN2.BackColor = System.Drawing.Color.Transparent
        Me.caballoN2.Image = Global.Ajedrez.My.Resources.Resources.caballoNegro
        Me.caballoN2.Location = New System.Drawing.Point(0, 0)
        Me.caballoN2.Margin = New System.Windows.Forms.Padding(0)
        Me.caballoN2.Name = "caballoN2"
        Me.caballoN2.Size = New System.Drawing.Size(50, 50)
        Me.caballoN2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.caballoN2.TabIndex = 119
        Me.caballoN2.TabStop = False
        '
        'arfilN2
        '
        Me.arfilN2.BackColor = System.Drawing.Color.Transparent
        Me.arfilN2.Image = Global.Ajedrez.My.Resources.Resources.arfilNegro
        Me.arfilN2.Location = New System.Drawing.Point(0, 0)
        Me.arfilN2.Margin = New System.Windows.Forms.Padding(0)
        Me.arfilN2.Name = "arfilN2"
        Me.arfilN2.Size = New System.Drawing.Size(50, 50)
        Me.arfilN2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.arfilN2.TabIndex = 118
        Me.arfilN2.TabStop = False
        '
        'reyN
        '
        Me.reyN.BackColor = System.Drawing.Color.Transparent
        Me.reyN.Image = Global.Ajedrez.My.Resources.Resources.reyNegro
        Me.reyN.Location = New System.Drawing.Point(0, 0)
        Me.reyN.Margin = New System.Windows.Forms.Padding(0)
        Me.reyN.Name = "reyN"
        Me.reyN.Size = New System.Drawing.Size(50, 50)
        Me.reyN.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.reyN.TabIndex = 117
        Me.reyN.TabStop = False
        '
        'reinaN
        '
        Me.reinaN.BackColor = System.Drawing.Color.Transparent
        Me.reinaN.Image = Global.Ajedrez.My.Resources.Resources.reinaNegra
        Me.reinaN.Location = New System.Drawing.Point(0, 0)
        Me.reinaN.Margin = New System.Windows.Forms.Padding(0)
        Me.reinaN.Name = "reinaN"
        Me.reinaN.Size = New System.Drawing.Size(50, 50)
        Me.reinaN.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.reinaN.TabIndex = 116
        Me.reinaN.TabStop = False
        '
        'arfilN1
        '
        Me.arfilN1.BackColor = System.Drawing.Color.Transparent
        Me.arfilN1.Image = Global.Ajedrez.My.Resources.Resources.arfilNegro
        Me.arfilN1.Location = New System.Drawing.Point(0, 0)
        Me.arfilN1.Margin = New System.Windows.Forms.Padding(0)
        Me.arfilN1.Name = "arfilN1"
        Me.arfilN1.Size = New System.Drawing.Size(50, 50)
        Me.arfilN1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.arfilN1.TabIndex = 115
        Me.arfilN1.TabStop = False
        '
        'caballoN1
        '
        Me.caballoN1.BackColor = System.Drawing.Color.Transparent
        Me.caballoN1.Image = Global.Ajedrez.My.Resources.Resources.caballoNegro
        Me.caballoN1.Location = New System.Drawing.Point(0, 0)
        Me.caballoN1.Margin = New System.Windows.Forms.Padding(0)
        Me.caballoN1.Name = "caballoN1"
        Me.caballoN1.Size = New System.Drawing.Size(50, 50)
        Me.caballoN1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.caballoN1.TabIndex = 114
        Me.caballoN1.TabStop = False
        '
        'torreN1
        '
        Me.torreN1.BackColor = System.Drawing.Color.Transparent
        Me.torreN1.Image = Global.Ajedrez.My.Resources.Resources.torreNegra
        Me.torreN1.Location = New System.Drawing.Point(0, 0)
        Me.torreN1.Margin = New System.Windows.Forms.Padding(0)
        Me.torreN1.Name = "torreN1"
        Me.torreN1.Size = New System.Drawing.Size(50, 50)
        Me.torreN1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.torreN1.TabIndex = 113
        Me.torreN1.TabStop = False
        '
        'auxiliar
        '
        Me.auxiliar.BackColor = System.Drawing.Color.Transparent
        Me.auxiliar.Location = New System.Drawing.Point(423, 12)
        Me.auxiliar.Name = "auxiliar"
        Me.auxiliar.Size = New System.Drawing.Size(17, 23)
        Me.auxiliar.TabIndex = 112
        Me.auxiliar.TabStop = False
        '
        'H3
        '
        Me.H3.BackColor = System.Drawing.Color.Transparent
        Me.H3.Location = New System.Drawing.Point(358, 259)
        Me.H3.Margin = New System.Windows.Forms.Padding(0)
        Me.H3.Name = "H3"
        Me.H3.Size = New System.Drawing.Size(50, 50)
        Me.H3.TabIndex = 111
        Me.H3.TabStop = False
        '
        'G3
        '
        Me.G3.BackColor = System.Drawing.Color.Transparent
        Me.G3.Location = New System.Drawing.Point(308, 259)
        Me.G3.Margin = New System.Windows.Forms.Padding(0)
        Me.G3.Name = "G3"
        Me.G3.Size = New System.Drawing.Size(50, 50)
        Me.G3.TabIndex = 110
        Me.G3.TabStop = False
        '
        'F3
        '
        Me.F3.BackColor = System.Drawing.Color.Transparent
        Me.F3.Location = New System.Drawing.Point(258, 259)
        Me.F3.Margin = New System.Windows.Forms.Padding(0)
        Me.F3.Name = "F3"
        Me.F3.Size = New System.Drawing.Size(50, 50)
        Me.F3.TabIndex = 109
        Me.F3.TabStop = False
        '
        'E3
        '
        Me.E3.BackColor = System.Drawing.Color.Transparent
        Me.E3.Location = New System.Drawing.Point(208, 259)
        Me.E3.Margin = New System.Windows.Forms.Padding(0)
        Me.E3.Name = "E3"
        Me.E3.Size = New System.Drawing.Size(50, 50)
        Me.E3.TabIndex = 108
        Me.E3.TabStop = False
        '
        'D3
        '
        Me.D3.BackColor = System.Drawing.Color.Transparent
        Me.D3.Location = New System.Drawing.Point(158, 259)
        Me.D3.Margin = New System.Windows.Forms.Padding(0)
        Me.D3.Name = "D3"
        Me.D3.Size = New System.Drawing.Size(50, 50)
        Me.D3.TabIndex = 107
        Me.D3.TabStop = False
        '
        'C3
        '
        Me.C3.BackColor = System.Drawing.Color.Transparent
        Me.C3.Location = New System.Drawing.Point(108, 259)
        Me.C3.Margin = New System.Windows.Forms.Padding(0)
        Me.C3.Name = "C3"
        Me.C3.Size = New System.Drawing.Size(50, 50)
        Me.C3.TabIndex = 106
        Me.C3.TabStop = False
        '
        'B3
        '
        Me.B3.BackColor = System.Drawing.Color.Transparent
        Me.B3.Location = New System.Drawing.Point(58, 259)
        Me.B3.Margin = New System.Windows.Forms.Padding(0)
        Me.B3.Name = "B3"
        Me.B3.Size = New System.Drawing.Size(50, 50)
        Me.B3.TabIndex = 105
        Me.B3.TabStop = False
        '
        'A3
        '
        Me.A3.BackColor = System.Drawing.Color.Transparent
        Me.A3.Location = New System.Drawing.Point(8, 259)
        Me.A3.Margin = New System.Windows.Forms.Padding(0)
        Me.A3.Name = "A3"
        Me.A3.Size = New System.Drawing.Size(50, 50)
        Me.A3.TabIndex = 104
        Me.A3.TabStop = False
        '
        'A4
        '
        Me.A4.BackColor = System.Drawing.Color.Transparent
        Me.A4.Location = New System.Drawing.Point(8, 209)
        Me.A4.Margin = New System.Windows.Forms.Padding(0)
        Me.A4.Name = "A4"
        Me.A4.Size = New System.Drawing.Size(50, 50)
        Me.A4.TabIndex = 103
        Me.A4.TabStop = False
        '
        'D4
        '
        Me.D4.BackColor = System.Drawing.Color.Transparent
        Me.D4.Location = New System.Drawing.Point(158, 209)
        Me.D4.Margin = New System.Windows.Forms.Padding(0)
        Me.D4.Name = "D4"
        Me.D4.Size = New System.Drawing.Size(50, 50)
        Me.D4.TabIndex = 102
        Me.D4.TabStop = False
        '
        'E4
        '
        Me.E4.BackColor = System.Drawing.Color.Transparent
        Me.E4.Location = New System.Drawing.Point(208, 209)
        Me.E4.Margin = New System.Windows.Forms.Padding(0)
        Me.E4.Name = "E4"
        Me.E4.Size = New System.Drawing.Size(50, 50)
        Me.E4.TabIndex = 101
        Me.E4.TabStop = False
        '
        'F4
        '
        Me.F4.BackColor = System.Drawing.Color.Transparent
        Me.F4.Location = New System.Drawing.Point(258, 209)
        Me.F4.Margin = New System.Windows.Forms.Padding(0)
        Me.F4.Name = "F4"
        Me.F4.Size = New System.Drawing.Size(50, 50)
        Me.F4.TabIndex = 100
        Me.F4.TabStop = False
        '
        'G4
        '
        Me.G4.BackColor = System.Drawing.Color.Transparent
        Me.G4.Location = New System.Drawing.Point(308, 209)
        Me.G4.Margin = New System.Windows.Forms.Padding(0)
        Me.G4.Name = "G4"
        Me.G4.Size = New System.Drawing.Size(50, 50)
        Me.G4.TabIndex = 99
        Me.G4.TabStop = False
        '
        'H4
        '
        Me.H4.BackColor = System.Drawing.Color.Transparent
        Me.H4.Location = New System.Drawing.Point(358, 209)
        Me.H4.Margin = New System.Windows.Forms.Padding(0)
        Me.H4.Name = "H4"
        Me.H4.Size = New System.Drawing.Size(50, 50)
        Me.H4.TabIndex = 98
        Me.H4.TabStop = False
        '
        'C4
        '
        Me.C4.BackColor = System.Drawing.Color.Transparent
        Me.C4.Location = New System.Drawing.Point(108, 209)
        Me.C4.Margin = New System.Windows.Forms.Padding(0)
        Me.C4.Name = "C4"
        Me.C4.Size = New System.Drawing.Size(50, 50)
        Me.C4.TabIndex = 97
        Me.C4.TabStop = False
        '
        'B4
        '
        Me.B4.BackColor = System.Drawing.Color.Transparent
        Me.B4.Location = New System.Drawing.Point(58, 209)
        Me.B4.Margin = New System.Windows.Forms.Padding(0)
        Me.B4.Name = "B4"
        Me.B4.Size = New System.Drawing.Size(50, 50)
        Me.B4.TabIndex = 96
        Me.B4.TabStop = False
        '
        'A5
        '
        Me.A5.BackColor = System.Drawing.Color.Transparent
        Me.A5.Location = New System.Drawing.Point(8, 159)
        Me.A5.Margin = New System.Windows.Forms.Padding(0)
        Me.A5.Name = "A5"
        Me.A5.Size = New System.Drawing.Size(50, 50)
        Me.A5.TabIndex = 95
        Me.A5.TabStop = False
        '
        'B5
        '
        Me.B5.BackColor = System.Drawing.Color.Transparent
        Me.B5.Location = New System.Drawing.Point(58, 159)
        Me.B5.Margin = New System.Windows.Forms.Padding(0)
        Me.B5.Name = "B5"
        Me.B5.Size = New System.Drawing.Size(50, 50)
        Me.B5.TabIndex = 94
        Me.B5.TabStop = False
        '
        'H5
        '
        Me.H5.BackColor = System.Drawing.Color.Transparent
        Me.H5.Location = New System.Drawing.Point(358, 159)
        Me.H5.Margin = New System.Windows.Forms.Padding(0)
        Me.H5.Name = "H5"
        Me.H5.Size = New System.Drawing.Size(50, 50)
        Me.H5.TabIndex = 93
        Me.H5.TabStop = False
        '
        'G5
        '
        Me.G5.BackColor = System.Drawing.Color.Transparent
        Me.G5.Location = New System.Drawing.Point(308, 159)
        Me.G5.Margin = New System.Windows.Forms.Padding(0)
        Me.G5.Name = "G5"
        Me.G5.Size = New System.Drawing.Size(50, 50)
        Me.G5.TabIndex = 92
        Me.G5.TabStop = False
        '
        'F5
        '
        Me.F5.BackColor = System.Drawing.Color.Transparent
        Me.F5.Location = New System.Drawing.Point(258, 159)
        Me.F5.Margin = New System.Windows.Forms.Padding(0)
        Me.F5.Name = "F5"
        Me.F5.Size = New System.Drawing.Size(50, 50)
        Me.F5.TabIndex = 91
        Me.F5.TabStop = False
        '
        'E5
        '
        Me.E5.BackColor = System.Drawing.Color.Transparent
        Me.E5.Location = New System.Drawing.Point(208, 159)
        Me.E5.Margin = New System.Windows.Forms.Padding(0)
        Me.E5.Name = "E5"
        Me.E5.Size = New System.Drawing.Size(50, 50)
        Me.E5.TabIndex = 90
        Me.E5.TabStop = False
        '
        'D5
        '
        Me.D5.BackColor = System.Drawing.Color.Transparent
        Me.D5.Location = New System.Drawing.Point(158, 159)
        Me.D5.Margin = New System.Windows.Forms.Padding(0)
        Me.D5.Name = "D5"
        Me.D5.Size = New System.Drawing.Size(50, 50)
        Me.D5.TabIndex = 89
        Me.D5.TabStop = False
        '
        'C5
        '
        Me.C5.BackColor = System.Drawing.Color.Transparent
        Me.C5.Location = New System.Drawing.Point(108, 159)
        Me.C5.Margin = New System.Windows.Forms.Padding(0)
        Me.C5.Name = "C5"
        Me.C5.Size = New System.Drawing.Size(50, 50)
        Me.C5.TabIndex = 88
        Me.C5.TabStop = False
        '
        'H6
        '
        Me.H6.BackColor = System.Drawing.Color.Transparent
        Me.H6.Location = New System.Drawing.Point(358, 109)
        Me.H6.Margin = New System.Windows.Forms.Padding(0)
        Me.H6.Name = "H6"
        Me.H6.Size = New System.Drawing.Size(50, 50)
        Me.H6.TabIndex = 87
        Me.H6.TabStop = False
        '
        'G6
        '
        Me.G6.BackColor = System.Drawing.Color.Transparent
        Me.G6.Location = New System.Drawing.Point(308, 109)
        Me.G6.Margin = New System.Windows.Forms.Padding(0)
        Me.G6.Name = "G6"
        Me.G6.Size = New System.Drawing.Size(50, 50)
        Me.G6.TabIndex = 86
        Me.G6.TabStop = False
        '
        'F6
        '
        Me.F6.BackColor = System.Drawing.Color.Transparent
        Me.F6.Location = New System.Drawing.Point(258, 109)
        Me.F6.Margin = New System.Windows.Forms.Padding(0)
        Me.F6.Name = "F6"
        Me.F6.Size = New System.Drawing.Size(50, 50)
        Me.F6.TabIndex = 85
        Me.F6.TabStop = False
        '
        'E6
        '
        Me.E6.BackColor = System.Drawing.Color.Transparent
        Me.E6.Location = New System.Drawing.Point(208, 109)
        Me.E6.Margin = New System.Windows.Forms.Padding(0)
        Me.E6.Name = "E6"
        Me.E6.Size = New System.Drawing.Size(50, 50)
        Me.E6.TabIndex = 84
        Me.E6.TabStop = False
        '
        'D6
        '
        Me.D6.BackColor = System.Drawing.Color.Transparent
        Me.D6.Location = New System.Drawing.Point(158, 109)
        Me.D6.Margin = New System.Windows.Forms.Padding(0)
        Me.D6.Name = "D6"
        Me.D6.Size = New System.Drawing.Size(50, 50)
        Me.D6.TabIndex = 83
        Me.D6.TabStop = False
        '
        'C6
        '
        Me.C6.BackColor = System.Drawing.Color.Transparent
        Me.C6.Location = New System.Drawing.Point(108, 109)
        Me.C6.Margin = New System.Windows.Forms.Padding(0)
        Me.C6.Name = "C6"
        Me.C6.Size = New System.Drawing.Size(50, 50)
        Me.C6.TabIndex = 82
        Me.C6.TabStop = False
        '
        'B6
        '
        Me.B6.BackColor = System.Drawing.Color.Transparent
        Me.B6.Location = New System.Drawing.Point(58, 109)
        Me.B6.Margin = New System.Windows.Forms.Padding(0)
        Me.B6.Name = "B6"
        Me.B6.Size = New System.Drawing.Size(50, 50)
        Me.B6.TabIndex = 81
        Me.B6.TabStop = False
        '
        'A6
        '
        Me.A6.BackColor = System.Drawing.Color.Transparent
        Me.A6.Location = New System.Drawing.Point(8, 109)
        Me.A6.Margin = New System.Windows.Forms.Padding(0)
        Me.A6.Name = "A6"
        Me.A6.Size = New System.Drawing.Size(50, 50)
        Me.A6.TabIndex = 80
        Me.A6.TabStop = False
        '
        'fotoJ2
        '
        Me.fotoJ2.BackColor = System.Drawing.Color.Transparent
        Me.fotoJ2.Image = CType(resources.GetObject("fotoJ2.Image"), System.Drawing.Image)
        Me.fotoJ2.Location = New System.Drawing.Point(443, 266)
        Me.fotoJ2.Margin = New System.Windows.Forms.Padding(0)
        Me.fotoJ2.Name = "fotoJ2"
        Me.fotoJ2.Size = New System.Drawing.Size(92, 99)
        Me.fotoJ2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.fotoJ2.TabIndex = 79
        Me.fotoJ2.TabStop = False
        '
        'fotoJ1
        '
        Me.fotoJ1.BackColor = System.Drawing.Color.Transparent
        Me.fotoJ1.Image = CType(resources.GetObject("fotoJ1.Image"), System.Drawing.Image)
        Me.fotoJ1.Location = New System.Drawing.Point(443, 16)
        Me.fotoJ1.Margin = New System.Windows.Forms.Padding(0)
        Me.fotoJ1.Name = "fotoJ1"
        Me.fotoJ1.Size = New System.Drawing.Size(92, 99)
        Me.fotoJ1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.fotoJ1.TabIndex = 78
        Me.fotoJ1.TabStop = False
        '
        'H1
        '
        Me.H1.BackColor = System.Drawing.Color.Transparent
        Me.H1.Location = New System.Drawing.Point(358, 359)
        Me.H1.Margin = New System.Windows.Forms.Padding(0)
        Me.H1.Name = "H1"
        Me.H1.Size = New System.Drawing.Size(50, 50)
        Me.H1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.H1.TabIndex = 77
        Me.H1.TabStop = False
        '
        'H2
        '
        Me.H2.BackColor = System.Drawing.Color.Transparent
        Me.H2.Location = New System.Drawing.Point(358, 309)
        Me.H2.Margin = New System.Windows.Forms.Padding(0)
        Me.H2.Name = "H2"
        Me.H2.Size = New System.Drawing.Size(50, 50)
        Me.H2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.H2.TabIndex = 76
        Me.H2.TabStop = False
        '
        'G2
        '
        Me.G2.BackColor = System.Drawing.Color.Transparent
        Me.G2.Location = New System.Drawing.Point(308, 309)
        Me.G2.Margin = New System.Windows.Forms.Padding(0)
        Me.G2.Name = "G2"
        Me.G2.Size = New System.Drawing.Size(50, 50)
        Me.G2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.G2.TabIndex = 75
        Me.G2.TabStop = False
        '
        'E2
        '
        Me.E2.BackColor = System.Drawing.Color.Transparent
        Me.E2.Location = New System.Drawing.Point(208, 309)
        Me.E2.Margin = New System.Windows.Forms.Padding(0)
        Me.E2.Name = "E2"
        Me.E2.Size = New System.Drawing.Size(50, 50)
        Me.E2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.E2.TabIndex = 74
        Me.E2.TabStop = False
        '
        'D2
        '
        Me.D2.BackColor = System.Drawing.Color.Transparent
        Me.D2.Location = New System.Drawing.Point(158, 309)
        Me.D2.Margin = New System.Windows.Forms.Padding(0)
        Me.D2.Name = "D2"
        Me.D2.Size = New System.Drawing.Size(50, 50)
        Me.D2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.D2.TabIndex = 73
        Me.D2.TabStop = False
        '
        'F2
        '
        Me.F2.BackColor = System.Drawing.Color.Transparent
        Me.F2.Location = New System.Drawing.Point(258, 309)
        Me.F2.Margin = New System.Windows.Forms.Padding(0)
        Me.F2.Name = "F2"
        Me.F2.Size = New System.Drawing.Size(50, 50)
        Me.F2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.F2.TabIndex = 73
        Me.F2.TabStop = False
        '
        'C2
        '
        Me.C2.BackColor = System.Drawing.Color.Transparent
        Me.C2.Location = New System.Drawing.Point(108, 309)
        Me.C2.Margin = New System.Windows.Forms.Padding(0)
        Me.C2.Name = "C2"
        Me.C2.Size = New System.Drawing.Size(50, 50)
        Me.C2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.C2.TabIndex = 72
        Me.C2.TabStop = False
        '
        'B2
        '
        Me.B2.BackColor = System.Drawing.Color.Transparent
        Me.B2.Location = New System.Drawing.Point(58, 309)
        Me.B2.Margin = New System.Windows.Forms.Padding(0)
        Me.B2.Name = "B2"
        Me.B2.Size = New System.Drawing.Size(50, 50)
        Me.B2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.B2.TabIndex = 71
        Me.B2.TabStop = False
        '
        'H7
        '
        Me.H7.BackColor = System.Drawing.Color.Transparent
        Me.H7.Location = New System.Drawing.Point(358, 59)
        Me.H7.Margin = New System.Windows.Forms.Padding(0)
        Me.H7.Name = "H7"
        Me.H7.Size = New System.Drawing.Size(50, 50)
        Me.H7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.H7.TabIndex = 70
        Me.H7.TabStop = False
        '
        'G7
        '
        Me.G7.BackColor = System.Drawing.Color.Transparent
        Me.G7.Location = New System.Drawing.Point(308, 59)
        Me.G7.Margin = New System.Windows.Forms.Padding(0)
        Me.G7.Name = "G7"
        Me.G7.Size = New System.Drawing.Size(50, 50)
        Me.G7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.G7.TabIndex = 69
        Me.G7.TabStop = False
        '
        'F7
        '
        Me.F7.BackColor = System.Drawing.Color.Transparent
        Me.F7.Location = New System.Drawing.Point(258, 59)
        Me.F7.Margin = New System.Windows.Forms.Padding(0)
        Me.F7.Name = "F7"
        Me.F7.Size = New System.Drawing.Size(50, 50)
        Me.F7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.F7.TabIndex = 68
        Me.F7.TabStop = False
        '
        'E7
        '
        Me.E7.BackColor = System.Drawing.Color.Transparent
        Me.E7.Location = New System.Drawing.Point(208, 59)
        Me.E7.Margin = New System.Windows.Forms.Padding(0)
        Me.E7.Name = "E7"
        Me.E7.Size = New System.Drawing.Size(50, 50)
        Me.E7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.E7.TabIndex = 67
        Me.E7.TabStop = False
        '
        'D7
        '
        Me.D7.BackColor = System.Drawing.Color.Transparent
        Me.D7.Location = New System.Drawing.Point(158, 59)
        Me.D7.Margin = New System.Windows.Forms.Padding(0)
        Me.D7.Name = "D7"
        Me.D7.Size = New System.Drawing.Size(50, 50)
        Me.D7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.D7.TabIndex = 66
        Me.D7.TabStop = False
        '
        'C7
        '
        Me.C7.BackColor = System.Drawing.Color.Transparent
        Me.C7.Location = New System.Drawing.Point(108, 59)
        Me.C7.Margin = New System.Windows.Forms.Padding(0)
        Me.C7.Name = "C7"
        Me.C7.Size = New System.Drawing.Size(50, 50)
        Me.C7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.C7.TabIndex = 65
        Me.C7.TabStop = False
        '
        'B7
        '
        Me.B7.BackColor = System.Drawing.Color.Transparent
        Me.B7.Location = New System.Drawing.Point(58, 59)
        Me.B7.Margin = New System.Windows.Forms.Padding(0)
        Me.B7.Name = "B7"
        Me.B7.Size = New System.Drawing.Size(50, 50)
        Me.B7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.B7.TabIndex = 64
        Me.B7.TabStop = False
        '
        'G8
        '
        Me.G8.BackColor = System.Drawing.Color.Transparent
        Me.G8.Location = New System.Drawing.Point(308, 9)
        Me.G8.Margin = New System.Windows.Forms.Padding(0)
        Me.G8.Name = "G8"
        Me.G8.Size = New System.Drawing.Size(50, 50)
        Me.G8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.G8.TabIndex = 63
        Me.G8.TabStop = False
        '
        'F8
        '
        Me.F8.BackColor = System.Drawing.Color.Transparent
        Me.F8.Location = New System.Drawing.Point(258, 9)
        Me.F8.Margin = New System.Windows.Forms.Padding(0)
        Me.F8.Name = "F8"
        Me.F8.Size = New System.Drawing.Size(50, 50)
        Me.F8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.F8.TabIndex = 62
        Me.F8.TabStop = False
        '
        'G1
        '
        Me.G1.BackColor = System.Drawing.Color.Transparent
        Me.G1.Location = New System.Drawing.Point(308, 359)
        Me.G1.Margin = New System.Windows.Forms.Padding(0)
        Me.G1.Name = "G1"
        Me.G1.Size = New System.Drawing.Size(50, 50)
        Me.G1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.G1.TabIndex = 55
        Me.G1.TabStop = False
        '
        'F1
        '
        Me.F1.BackColor = System.Drawing.Color.Transparent
        Me.F1.Location = New System.Drawing.Point(258, 359)
        Me.F1.Margin = New System.Windows.Forms.Padding(0)
        Me.F1.Name = "F1"
        Me.F1.Size = New System.Drawing.Size(50, 50)
        Me.F1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.F1.TabIndex = 54
        Me.F1.TabStop = False
        '
        'E1
        '
        Me.E1.BackColor = System.Drawing.Color.Transparent
        Me.E1.Location = New System.Drawing.Point(208, 359)
        Me.E1.Margin = New System.Windows.Forms.Padding(0)
        Me.E1.Name = "E1"
        Me.E1.Size = New System.Drawing.Size(50, 50)
        Me.E1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.E1.TabIndex = 53
        Me.E1.TabStop = False
        '
        'D1
        '
        Me.D1.BackColor = System.Drawing.Color.Transparent
        Me.D1.Location = New System.Drawing.Point(158, 359)
        Me.D1.Margin = New System.Windows.Forms.Padding(0)
        Me.D1.Name = "D1"
        Me.D1.Size = New System.Drawing.Size(50, 50)
        Me.D1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.D1.TabIndex = 52
        Me.D1.TabStop = False
        '
        'C1
        '
        Me.C1.BackColor = System.Drawing.Color.Transparent
        Me.C1.Location = New System.Drawing.Point(108, 359)
        Me.C1.Margin = New System.Windows.Forms.Padding(0)
        Me.C1.Name = "C1"
        Me.C1.Size = New System.Drawing.Size(50, 50)
        Me.C1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.C1.TabIndex = 51
        Me.C1.TabStop = False
        '
        'A2
        '
        Me.A2.BackColor = System.Drawing.Color.Transparent
        Me.A2.Location = New System.Drawing.Point(8, 309)
        Me.A2.Margin = New System.Windows.Forms.Padding(0)
        Me.A2.Name = "A2"
        Me.A2.Size = New System.Drawing.Size(50, 50)
        Me.A2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.A2.TabIndex = 49
        Me.A2.TabStop = False
        '
        'B1
        '
        Me.B1.BackColor = System.Drawing.Color.Transparent
        Me.B1.Location = New System.Drawing.Point(58, 359)
        Me.B1.Margin = New System.Windows.Forms.Padding(0)
        Me.B1.Name = "B1"
        Me.B1.Size = New System.Drawing.Size(50, 50)
        Me.B1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.B1.TabIndex = 48
        Me.B1.TabStop = False
        '
        'A7
        '
        Me.A7.BackColor = System.Drawing.Color.Transparent
        Me.A7.Location = New System.Drawing.Point(8, 59)
        Me.A7.Margin = New System.Windows.Forms.Padding(0)
        Me.A7.Name = "A7"
        Me.A7.Size = New System.Drawing.Size(50, 50)
        Me.A7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.A7.TabIndex = 40
        Me.A7.TabStop = False
        '
        'A1
        '
        Me.A1.BackColor = System.Drawing.Color.Transparent
        Me.A1.Location = New System.Drawing.Point(8, 359)
        Me.A1.Margin = New System.Windows.Forms.Padding(0)
        Me.A1.Name = "A1"
        Me.A1.Size = New System.Drawing.Size(50, 50)
        Me.A1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.A1.TabIndex = 38
        Me.A1.TabStop = False
        '
        'H8
        '
        Me.H8.BackColor = System.Drawing.Color.Transparent
        Me.H8.Location = New System.Drawing.Point(358, 9)
        Me.H8.Margin = New System.Windows.Forms.Padding(0)
        Me.H8.Name = "H8"
        Me.H8.Size = New System.Drawing.Size(50, 50)
        Me.H8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.H8.TabIndex = 37
        Me.H8.TabStop = False
        '
        'E8
        '
        Me.E8.BackColor = System.Drawing.Color.Transparent
        Me.E8.Location = New System.Drawing.Point(208, 9)
        Me.E8.Margin = New System.Windows.Forms.Padding(0)
        Me.E8.Name = "E8"
        Me.E8.Size = New System.Drawing.Size(50, 50)
        Me.E8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.E8.TabIndex = 34
        Me.E8.TabStop = False
        '
        'D8
        '
        Me.D8.BackColor = System.Drawing.Color.Transparent
        Me.D8.Location = New System.Drawing.Point(158, 9)
        Me.D8.Margin = New System.Windows.Forms.Padding(0)
        Me.D8.Name = "D8"
        Me.D8.Size = New System.Drawing.Size(50, 50)
        Me.D8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.D8.TabIndex = 33
        Me.D8.TabStop = False
        '
        'C8
        '
        Me.C8.BackColor = System.Drawing.Color.Transparent
        Me.C8.Location = New System.Drawing.Point(108, 9)
        Me.C8.Margin = New System.Windows.Forms.Padding(0)
        Me.C8.Name = "C8"
        Me.C8.Size = New System.Drawing.Size(50, 50)
        Me.C8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.C8.TabIndex = 32
        Me.C8.TabStop = False
        '
        'B8
        '
        Me.B8.BackColor = System.Drawing.Color.Transparent
        Me.B8.Location = New System.Drawing.Point(58, 9)
        Me.B8.Margin = New System.Windows.Forms.Padding(0)
        Me.B8.Name = "B8"
        Me.B8.Size = New System.Drawing.Size(50, 50)
        Me.B8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.B8.TabIndex = 31
        Me.B8.TabStop = False
        '
        'A8
        '
        Me.A8.BackColor = System.Drawing.Color.Transparent
        Me.A8.Location = New System.Drawing.Point(8, 9)
        Me.A8.Margin = New System.Windows.Forms.Padding(0)
        Me.A8.Name = "A8"
        Me.A8.Size = New System.Drawing.Size(50, 50)
        Me.A8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.A8.TabIndex = 30
        Me.A8.TabStop = False
        '
        'tablero
        '
        Me.tablero.BackColor = System.Drawing.SystemColors.Control
        Me.tablero.Image = Global.Ajedrez.My.Resources.Resources.capturilla
        Me.tablero.Location = New System.Drawing.Point(0, 0)
        Me.tablero.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.tablero.Name = "tablero"
        Me.tablero.Size = New System.Drawing.Size(418, 414)
        Me.tablero.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.tablero.TabIndex = 0
        Me.tablero.TabStop = False
        '
        'cementerio
        '
        Me.cementerio.BackColor = System.Drawing.Color.Transparent
        Me.cementerio.Image = CType(resources.GetObject("cementerio.Image"), System.Drawing.Image)
        Me.cementerio.Location = New System.Drawing.Point(443, 160)
        Me.cementerio.Margin = New System.Windows.Forms.Padding(0)
        Me.cementerio.Name = "cementerio"
        Me.cementerio.Size = New System.Drawing.Size(147, 99)
        Me.cementerio.TabIndex = 145
        Me.cementerio.TabStop = False
        '
        'cementeriTexto
        '
        Me.cementeriTexto.AutoSize = True
        Me.cementeriTexto.Location = New System.Drawing.Point(487, 196)
        Me.cementeriTexto.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.cementeriTexto.Name = "cementeriTexto"
        Me.cementeriTexto.Size = New System.Drawing.Size(60, 13)
        Me.cementeriTexto.TabIndex = 146
        Me.cementeriTexto.Text = "Cementerio"
        '
        'titulo
        '
        Me.titulo.AutoSize = True
        Me.titulo.Location = New System.Drawing.Point(270, 133)
        Me.titulo.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.titulo.Name = "titulo"
        Me.titulo.Size = New System.Drawing.Size(56, 13)
        Me.titulo.TabIndex = 147
        Me.titulo.Text = "AJEDREZ"
        '
        'comenzar
        '
        Me.comenzar.Location = New System.Drawing.Point(222, 283)
        Me.comenzar.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.comenzar.Name = "comenzar"
        Me.comenzar.Size = New System.Drawing.Size(147, 38)
        Me.comenzar.TabIndex = 148
        Me.comenzar.Text = "Comenzar"
        Me.comenzar.UseVisualStyleBackColor = True
        '
        'PantallaInicio
        '
        Me.PantallaInicio.Image = CType(resources.GetObject("PantallaInicio.Image"), System.Drawing.Image)
        Me.PantallaInicio.Location = New System.Drawing.Point(0, 0)
        Me.PantallaInicio.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.PantallaInicio.Name = "PantallaInicio"
        Me.PantallaInicio.Size = New System.Drawing.Size(601, 421)
        Me.PantallaInicio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PantallaInicio.TabIndex = 149
        Me.PantallaInicio.TabStop = False
        '
        'IntrJugador1
        '
        Me.IntrJugador1.Location = New System.Drawing.Point(82, 239)
        Me.IntrJugador1.Name = "IntrJugador1"
        Me.IntrJugador1.Size = New System.Drawing.Size(151, 20)
        Me.IntrJugador1.TabIndex = 150
        Me.IntrJugador1.Text = "Jugador 1"
        '
        'TextJugador1
        '
        Me.TextJugador1.AutoSize = True
        Me.TextJugador1.Location = New System.Drawing.Point(78, 196)
        Me.TextJugador1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.TextJugador1.Name = "TextJugador1"
        Me.TextJugador1.Size = New System.Drawing.Size(155, 13)
        Me.TextJugador1.TabIndex = 151
        Me.TextJugador1.Text = "Introduce nombre de Jugador 1"
        '
        'TextJugador2
        '
        Me.TextJugador2.AutoSize = True
        Me.TextJugador2.Location = New System.Drawing.Point(360, 196)
        Me.TextJugador2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.TextJugador2.Name = "TextJugador2"
        Me.TextJugador2.Size = New System.Drawing.Size(155, 13)
        Me.TextJugador2.TabIndex = 152
        Me.TextJugador2.Text = "Introduce nombre de Jugador 2"
        '
        'IntrJugador2
        '
        Me.IntrJugador2.Location = New System.Drawing.Point(358, 239)
        Me.IntrJugador2.Name = "IntrJugador2"
        Me.IntrJugador2.Size = New System.Drawing.Size(151, 20)
        Me.IntrJugador2.TabIndex = 153
        Me.IntrJugador2.Text = "Jugador 2"
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(601, 424)
        Me.PictureBox1.TabIndex = 154
        Me.PictureBox1.TabStop = False
        '
        'mesa
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(598, 421)
        Me.Controls.Add(Me.IntrJugador2)
        Me.Controls.Add(Me.TextJugador2)
        Me.Controls.Add(Me.TextJugador1)
        Me.Controls.Add(Me.IntrJugador1)
        Me.Controls.Add(Me.comenzar)
        Me.Controls.Add(Me.titulo)
        Me.Controls.Add(Me.PantallaInicio)
        Me.Controls.Add(Me.cementeriTexto)
        Me.Controls.Add(Me.cementerio)
        Me.Controls.Add(Me.torreB2)
        Me.Controls.Add(Me.caballoB2)
        Me.Controls.Add(Me.arfilB2)
        Me.Controls.Add(Me.reyB)
        Me.Controls.Add(Me.reinaB)
        Me.Controls.Add(Me.arfilB1)
        Me.Controls.Add(Me.caballoB1)
        Me.Controls.Add(Me.torreB1)
        Me.Controls.Add(Me.peonB8)
        Me.Controls.Add(Me.peonB7)
        Me.Controls.Add(Me.peonB6)
        Me.Controls.Add(Me.peonB5)
        Me.Controls.Add(Me.peonB4)
        Me.Controls.Add(Me.peonB3)
        Me.Controls.Add(Me.peonB2)
        Me.Controls.Add(Me.peonB1)
        Me.Controls.Add(Me.peonN8)
        Me.Controls.Add(Me.peonN7)
        Me.Controls.Add(Me.peonN6)
        Me.Controls.Add(Me.peonN5)
        Me.Controls.Add(Me.peonN4)
        Me.Controls.Add(Me.peonN3)
        Me.Controls.Add(Me.peonN2)
        Me.Controls.Add(Me.peonN1)
        Me.Controls.Add(Me.torreN2)
        Me.Controls.Add(Me.caballoN2)
        Me.Controls.Add(Me.arfilN2)
        Me.Controls.Add(Me.reyN)
        Me.Controls.Add(Me.reinaN)
        Me.Controls.Add(Me.arfilN1)
        Me.Controls.Add(Me.caballoN1)
        Me.Controls.Add(Me.torreN1)
        Me.Controls.Add(Me.auxiliar)
        Me.Controls.Add(Me.H3)
        Me.Controls.Add(Me.G3)
        Me.Controls.Add(Me.F3)
        Me.Controls.Add(Me.E3)
        Me.Controls.Add(Me.D3)
        Me.Controls.Add(Me.C3)
        Me.Controls.Add(Me.B3)
        Me.Controls.Add(Me.A3)
        Me.Controls.Add(Me.A4)
        Me.Controls.Add(Me.D4)
        Me.Controls.Add(Me.E4)
        Me.Controls.Add(Me.F4)
        Me.Controls.Add(Me.G4)
        Me.Controls.Add(Me.H4)
        Me.Controls.Add(Me.C4)
        Me.Controls.Add(Me.B4)
        Me.Controls.Add(Me.A5)
        Me.Controls.Add(Me.B5)
        Me.Controls.Add(Me.H5)
        Me.Controls.Add(Me.G5)
        Me.Controls.Add(Me.F5)
        Me.Controls.Add(Me.E5)
        Me.Controls.Add(Me.D5)
        Me.Controls.Add(Me.C5)
        Me.Controls.Add(Me.H6)
        Me.Controls.Add(Me.G6)
        Me.Controls.Add(Me.F6)
        Me.Controls.Add(Me.E6)
        Me.Controls.Add(Me.D6)
        Me.Controls.Add(Me.C6)
        Me.Controls.Add(Me.B6)
        Me.Controls.Add(Me.A6)
        Me.Controls.Add(Me.fotoJ2)
        Me.Controls.Add(Me.fotoJ1)
        Me.Controls.Add(Me.H1)
        Me.Controls.Add(Me.H2)
        Me.Controls.Add(Me.G2)
        Me.Controls.Add(Me.E2)
        Me.Controls.Add(Me.D2)
        Me.Controls.Add(Me.F2)
        Me.Controls.Add(Me.C2)
        Me.Controls.Add(Me.B2)
        Me.Controls.Add(Me.H7)
        Me.Controls.Add(Me.G7)
        Me.Controls.Add(Me.F7)
        Me.Controls.Add(Me.E7)
        Me.Controls.Add(Me.D7)
        Me.Controls.Add(Me.C7)
        Me.Controls.Add(Me.B7)
        Me.Controls.Add(Me.G8)
        Me.Controls.Add(Me.F8)
        Me.Controls.Add(Me.G1)
        Me.Controls.Add(Me.F1)
        Me.Controls.Add(Me.E1)
        Me.Controls.Add(Me.D1)
        Me.Controls.Add(Me.C1)
        Me.Controls.Add(Me.A2)
        Me.Controls.Add(Me.B1)
        Me.Controls.Add(Me.A7)
        Me.Controls.Add(Me.A1)
        Me.Controls.Add(Me.H8)
        Me.Controls.Add(Me.E8)
        Me.Controls.Add(Me.D8)
        Me.Controls.Add(Me.C8)
        Me.Controls.Add(Me.B8)
        Me.Controls.Add(Me.A8)
        Me.Controls.Add(Me.jugador2)
        Me.Controls.Add(Me.jugador1)
        Me.Controls.Add(Me.rendirseB)
        Me.Controls.Add(Me.rendirseN)
        Me.Controls.Add(Me.tablero)
        Me.Controls.Add(Me.PictureBox1)
        Me.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.Name = "mesa"
        Me.Text = "Form1"
        CType(Me.torreB2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.caballoB2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.arfilB2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.reyB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.reinaB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.arfilB1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.caballoB1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.torreB1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.peonB8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.peonB7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.peonB6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.peonB5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.peonB4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.peonB3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.peonB2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.peonB1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.peonN8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.peonN7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.peonN6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.peonN5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.peonN4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.peonN3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.peonN2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.peonN1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.torreN2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.caballoN2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.arfilN2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.reyN, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.reinaN, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.arfilN1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.caballoN1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.torreN1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.auxiliar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.H3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.G3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.F3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.E3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.D3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.B3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.A3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.A4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.D4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.E4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.F4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.G4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.H4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.B4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.A5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.B5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.H5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.G5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.F5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.E5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.D5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.H6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.G6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.F6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.E6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.D6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.B6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.A6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.fotoJ2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.fotoJ1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.H1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.H2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.G2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.E2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.D2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.F2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.B2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.H7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.G7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.F7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.E7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.D7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.B7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.G8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.F8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.G1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.F1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.E1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.D1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.A2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.B1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.A7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.A1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.H8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.E8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.D8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.B8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.A8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tablero, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cementerio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PantallaInicio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents tablero As PictureBox
    Friend WithEvents rendirseN As Button
    Friend WithEvents rendirseB As Button
    Friend WithEvents jugador1 As Label
    Friend WithEvents jugador2 As Label
    Friend WithEvents A8 As PictureBox
    Friend WithEvents B8 As PictureBox
    Friend WithEvents C8 As PictureBox
    Friend WithEvents D8 As PictureBox
    Friend WithEvents E8 As PictureBox
    Friend WithEvents H8 As PictureBox
    Friend WithEvents A1 As PictureBox
    Friend WithEvents A7 As PictureBox
    Friend WithEvents B1 As PictureBox
    Friend WithEvents A2 As PictureBox
    Friend WithEvents C1 As PictureBox
    Friend WithEvents D1 As PictureBox
    Friend WithEvents E1 As PictureBox
    Friend WithEvents F1 As PictureBox
    Friend WithEvents G1 As PictureBox
    Friend WithEvents F8 As PictureBox
    Friend WithEvents G8 As PictureBox
    Friend WithEvents B7 As PictureBox
    Friend WithEvents C7 As PictureBox
    Friend WithEvents D7 As PictureBox
    Friend WithEvents E7 As PictureBox
    Friend WithEvents F7 As PictureBox
    Friend WithEvents G7 As PictureBox
    Friend WithEvents H7 As PictureBox
    Friend WithEvents B2 As PictureBox
    Friend WithEvents C2 As PictureBox
    Friend WithEvents F2 As PictureBox
    Friend WithEvents D2 As PictureBox
    Friend WithEvents E2 As PictureBox
    Friend WithEvents G2 As PictureBox
    Friend WithEvents H2 As PictureBox
    Friend WithEvents H1 As PictureBox
    Friend WithEvents fotoJ1 As PictureBox
    Friend WithEvents fotoJ2 As PictureBox
    Friend WithEvents A6 As PictureBox
    Friend WithEvents B6 As PictureBox
    Friend WithEvents C6 As PictureBox
    Friend WithEvents D6 As PictureBox
    Friend WithEvents E6 As PictureBox
    Friend WithEvents F6 As PictureBox
    Friend WithEvents G6 As PictureBox
    Friend WithEvents H6 As PictureBox
    Friend WithEvents C5 As PictureBox
    Friend WithEvents D5 As PictureBox
    Friend WithEvents E5 As PictureBox
    Friend WithEvents F5 As PictureBox
    Friend WithEvents G5 As PictureBox
    Friend WithEvents H5 As PictureBox
    Friend WithEvents B5 As PictureBox
    Friend WithEvents A5 As PictureBox
    Friend WithEvents B4 As PictureBox
    Friend WithEvents C4 As PictureBox
    Friend WithEvents H4 As PictureBox
    Friend WithEvents G4 As PictureBox
    Friend WithEvents F4 As PictureBox
    Friend WithEvents E4 As PictureBox
    Friend WithEvents D4 As PictureBox
    Friend WithEvents A4 As PictureBox
    Friend WithEvents A3 As PictureBox
    Friend WithEvents B3 As PictureBox
    Friend WithEvents C3 As PictureBox
    Friend WithEvents D3 As PictureBox
    Friend WithEvents E3 As PictureBox
    Friend WithEvents F3 As PictureBox
    Friend WithEvents G3 As PictureBox
    Friend WithEvents H3 As PictureBox
    Friend WithEvents auxiliar As PictureBox
    Friend WithEvents torreN1 As PictureBox
    Friend WithEvents caballoN1 As PictureBox
    Friend WithEvents arfilN1 As PictureBox
    Friend WithEvents reinaN As PictureBox
    Friend WithEvents reyN As PictureBox
    Friend WithEvents arfilN2 As PictureBox
    Friend WithEvents caballoN2 As PictureBox
    Friend WithEvents torreN2 As PictureBox
    Friend WithEvents peonN1 As PictureBox
    Friend WithEvents peonN2 As PictureBox
    Friend WithEvents peonN3 As PictureBox
    Friend WithEvents peonN4 As PictureBox
    Friend WithEvents peonN5 As PictureBox
    Friend WithEvents peonN6 As PictureBox
    Friend WithEvents peonN7 As PictureBox
    Friend WithEvents peonN8 As PictureBox
    Friend WithEvents peonB1 As PictureBox
    Friend WithEvents peonB2 As PictureBox
    Friend WithEvents peonB3 As PictureBox
    Friend WithEvents peonB4 As PictureBox
    Friend WithEvents peonB5 As PictureBox
    Friend WithEvents peonB6 As PictureBox
    Friend WithEvents peonB7 As PictureBox
    Friend WithEvents peonB8 As PictureBox
    Friend WithEvents torreB1 As PictureBox
    Friend WithEvents caballoB1 As PictureBox
    Friend WithEvents arfilB1 As PictureBox
    Friend WithEvents reinaB As PictureBox
    Friend WithEvents reyB As PictureBox
    Friend WithEvents arfilB2 As PictureBox
    Friend WithEvents caballoB2 As PictureBox
    Friend WithEvents torreB2 As PictureBox
    Friend WithEvents cementerio As PictureBox
    Friend WithEvents cementeriTexto As Label
    Friend WithEvents titulo As Label
    Friend WithEvents comenzar As Button
    Friend WithEvents PantallaInicio As PictureBox
    Friend WithEvents IntrJugador1 As TextBox
    Friend WithEvents TextJugador1 As Label
    Friend WithEvents TextJugador2 As Label
    Friend WithEvents IntrJugador2 As TextBox
    Friend WithEvents PictureBox1 As PictureBox
End Class

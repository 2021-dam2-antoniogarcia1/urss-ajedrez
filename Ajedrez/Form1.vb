﻿Public Class mesa

    Private Sub rendirseN_Click(sender As Object, e As EventArgs) Handles rendirseN.Click
        Dim response As MsgBoxResult
        response = MsgBox(Convert.ToString("Ganador: " + jugador2.Text), MsgBoxStyle.Information, "Victoria!!")
    End Sub

    Private Sub rendirseB_Click(sender As Object, e As EventArgs) Handles rendirseB.Click
        Dim response As MsgBoxResult
        response = MsgBox(Convert.ToString("Ganador: " + jugador1.Text), MsgBoxStyle.Information, "Victoria!!")
    End Sub

    Dim casillas() As PictureBox
    Dim piezas() As PictureBox
    Dim inicio() As Label
    Dim pulsada As Int32
    Dim i As Int32
    Dim j As Int32

    Private Function colocacion()
        For i = 0 To 2
            inicio(i).Parent = PantallaInicio
        Next i
        i = 16
        For j = 0 To 63
            If j <= 15 Then
                piezas(j).Parent = casillas(j)
                piezas(j).BringToFront()
            End If
            If j >= 48 Then
                piezas(i).Parent = casillas(j)
                piezas(i).BringToFront()
                i = i + 1
            End If
            casillas(j).Parent = tablero
        Next j
        Return 1
    End Function

    Private Sub mesa_Load(sender As Object, e As EventArgs) Handles MyBase.Load


        inicio = New Label() {titulo, TextJugador1, TextJugador2}
        piezas = New PictureBox() {torreN1, caballoN1, arfilN1, reinaN, reyN, arfilN2, caballoN2, torreN2, peonN1, peonN2, peonN3, peonN4, peonN5, peonN6, peonN7, peonN8, peonB1, peonB2, peonB3, peonB4, peonB5, peonB6, peonB7, peonB8, torreB1, caballoB1, arfilB1, reinaB, reyB, arfilB2, caballoB2, torreB2}
        casillas = New PictureBox() {A8, B8, C8, D8, E8, F8, G8, H8, A7, B7, C7, D7, E7, F7, G7, H7, A6, B6, C6, D6, E6, F6, G6, H6, A5, B5, C5, D5, E5, F5, G5, H5, A4, B4, C4, D4, E4, F4, G4, H4, A3, B3, C3, D3, E3, F3, G3, H3, A2, B2, C2, D2, E2, F2, G2, H2, A1, B1, C1, D1, E1, F1, G1, H1}
        IntrJugador1.Parent = PantallaInicio
        IntrJugador2.Parent = PantallaInicio
        comenzar.Parent = PantallaInicio
        colocacion()


    End Sub



    Private Sub torreN1_MouseClick(sender As Object, e As MouseEventArgs) Handles torreN1.MouseClick
        pulsada = 0
    End Sub

    Private Sub caballoN1_MouseClick(sender As Object, e As MouseEventArgs) Handles caballoN1.MouseClick
        pulsada = 1
    End Sub

    Private Sub arfilN1_MouseClick(sender As Object, e As MouseEventArgs) Handles arfilN1.MouseClick
        pulsada = 2
    End Sub

    Private Sub reinaN_MouseClick(sender As Object, e As MouseEventArgs) Handles reinaN.MouseClick
        pulsada = 3
    End Sub

    Private Sub reyN_MouseClick(sender As Object, e As MouseEventArgs) Handles reyN.MouseClick
        pulsada = 4
    End Sub

    Private Sub arfilN2_MouseClick(sender As Object, e As MouseEventArgs) Handles arfilN2.MouseClick
        pulsada = 5
    End Sub

    Private Sub caballoN2_MouseClick(sender As Object, e As MouseEventArgs) Handles caballoN2.MouseClick
        pulsada = 6
    End Sub

    Private Sub torreN2_MouseClick(sender As Object, e As MouseEventArgs) Handles torreN2.MouseClick
        pulsada = 7
    End Sub

    Private Sub peonN1_MouseClick(sender As Object, e As MouseEventArgs) Handles peonN1.MouseClick
        pulsada = 8
    End Sub

    Private Sub peonN2_MouseClick(sender As Object, e As MouseEventArgs) Handles peonN2.MouseClick
        pulsada = 9
    End Sub

    Private Sub peonN3_MouseClick(sender As Object, e As MouseEventArgs) Handles peonN3.MouseClick
        pulsada = 10
    End Sub

    Private Sub peonN4_MouseClick(sender As Object, e As MouseEventArgs) Handles peonN4.MouseClick
        pulsada = 11
    End Sub

    Private Sub peonN5_MouseClick(sender As Object, e As MouseEventArgs) Handles peonN5.MouseClick
        pulsada = 12
    End Sub

    Private Sub peonN6_MouseClick(sender As Object, e As MouseEventArgs) Handles peonN6.MouseClick
        pulsada = 13
    End Sub

    Private Sub peonN7_MouseClick(sender As Object, e As MouseEventArgs) Handles peonN7.MouseClick
        pulsada = 14
    End Sub

    Private Sub peonN8_MouseClick(sender As Object, e As MouseEventArgs) Handles peonN8.MouseClick
        pulsada = 15
    End Sub

    Private Sub peonB1_MouseClick(sender As Object, e As MouseEventArgs) Handles peonB1.MouseClick
        pulsada = 16
    End Sub

    Private Sub peonB2_MouseClick(sender As Object, e As MouseEventArgs) Handles peonB2.MouseClick
        pulsada = 17
    End Sub

    Private Sub peonB3_MouseClick(sender As Object, e As MouseEventArgs) Handles peonB3.MouseClick
        pulsada = 18
    End Sub

    Private Sub peonB4_MouseClick(sender As Object, e As MouseEventArgs) Handles peonB4.MouseClick
        pulsada = 19
    End Sub

    Private Sub peonB5_MouseClick(sender As Object, e As MouseEventArgs) Handles peonB5.MouseClick
        pulsada = 20
    End Sub

    Private Sub peonB6_MouseClick(sender As Object, e As MouseEventArgs) Handles peonB6.MouseClick
        pulsada = 21
    End Sub

    Private Sub peonB7_MouseClick(sender As Object, e As MouseEventArgs) Handles peonB7.MouseClick
        pulsada = 22
    End Sub

    Private Sub peonB8_MouseClick(sender As Object, e As MouseEventArgs) Handles peonB8.MouseClick
        pulsada = 23
    End Sub

    Private Sub torreB1_MouseClick(sender As Object, e As MouseEventArgs) Handles torreB1.MouseClick
        pulsada = 24
    End Sub

    Private Sub caballoB1_MouseClick(sender As Object, e As MouseEventArgs) Handles caballoB1.MouseClick
        pulsada = 25
    End Sub

    Private Sub arfilB1_MouseClick(sender As Object, e As MouseEventArgs) Handles arfilB1.MouseClick
        pulsada = 26
    End Sub

    Private Sub reinaB_MouseClick(sender As Object, e As MouseEventArgs) Handles reinaB.MouseClick
        pulsada = 27
    End Sub

    Private Sub reyB_MouseClick(sender As Object, e As MouseEventArgs) Handles reyB.MouseClick
        pulsada = 28
    End Sub

    Private Sub arfilB2_MouseClick(sender As Object, e As MouseEventArgs) Handles arfilB2.MouseClick
        pulsada = 29
    End Sub

    Private Sub caballoB2_MouseClick(sender As Object, e As MouseEventArgs) Handles caballoB2.MouseClick
        pulsada = 30
    End Sub

    Private Sub torreB2_MouseClick(sender As Object, e As MouseEventArgs) Handles torreB2.MouseClick
        pulsada = 31
    End Sub

    Private Sub cementerio_MouseClick(sender As Object, e As MouseEventArgs) Handles cementerio.MouseClick
        pulsada = 32
    End Sub

    Private Sub A1_Click(sender As Object, e As EventArgs) Handles A1.Click
        piezas(pulsada).Parent = A1
    End Sub

    Private Sub A2_Click(sender As Object, e As EventArgs) Handles A2.Click
        piezas(pulsada).Parent = A2
    End Sub

    Private Sub A3_Click(sender As Object, e As EventArgs) Handles A3.Click
        piezas(pulsada).Parent = A3
    End Sub

    Private Sub A4_Click(sender As Object, e As EventArgs) Handles A4.Click
        piezas(pulsada).Parent = A4
    End Sub

    Private Sub A5_Click(sender As Object, e As EventArgs) Handles A5.Click
        piezas(pulsada).Parent = A5
    End Sub

    Private Sub A6_Click(sender As Object, e As EventArgs) Handles A6.Click
        piezas(pulsada).Parent = A6
    End Sub

    Private Sub A7_Click(sender As Object, e As EventArgs) Handles A7.Click
        piezas(pulsada).Parent = A7
    End Sub

    Private Sub A8_Click(sender As Object, e As EventArgs) Handles A8.Click
        piezas(pulsada).Parent = A8
    End Sub

    Private Sub B1_Click(sender As Object, e As EventArgs) Handles B1.Click
        piezas(pulsada).Parent = B1
    End Sub

    Private Sub B2_Click(sender As Object, e As EventArgs) Handles B2.Click
        piezas(pulsada).Parent = B2
    End Sub

    Private Sub B3_Click(sender As Object, e As EventArgs) Handles B3.Click
        piezas(pulsada).Parent = B3
    End Sub

    Private Sub B4_Click(sender As Object, e As EventArgs) Handles B4.Click
        piezas(pulsada).Parent = B4
    End Sub

    Private Sub B5_Click(sender As Object, e As EventArgs) Handles B5.Click
        piezas(pulsada).Parent = B5
    End Sub

    Private Sub B6_Click(sender As Object, e As EventArgs) Handles B6.Click
        piezas(pulsada).Parent = B6
    End Sub

    Private Sub B7_Click(sender As Object, e As EventArgs) Handles B7.Click
        piezas(pulsada).Parent = B7
    End Sub

    Private Sub B8_Click(sender As Object, e As EventArgs) Handles B8.Click
        piezas(pulsada).Parent = B8
    End Sub

    Private Sub C1_Click(sender As Object, e As EventArgs) Handles C1.Click
        piezas(pulsada).Parent = C1
    End Sub

    Private Sub C2_Click(sender As Object, e As EventArgs) Handles C2.Click
        piezas(pulsada).Parent = C2
    End Sub

    Private Sub C3_Click(sender As Object, e As EventArgs) Handles C3.Click
        piezas(pulsada).Parent = C3
    End Sub

    Private Sub C4_Click(sender As Object, e As EventArgs) Handles C4.Click
        piezas(pulsada).Parent = C4
    End Sub

    Private Sub C5_Click(sender As Object, e As EventArgs) Handles C5.Click
        piezas(pulsada).Parent = C5
    End Sub

    Private Sub C6_Click(sender As Object, e As EventArgs) Handles C6.Click
        piezas(pulsada).Parent = C6
    End Sub

    Private Sub C7_Click(sender As Object, e As EventArgs) Handles C7.Click
        piezas(pulsada).Parent = C7
    End Sub

    Private Sub C8_Click(sender As Object, e As EventArgs) Handles C8.Click
        piezas(pulsada).Parent = C8
    End Sub

    Private Sub D1_Click(sender As Object, e As EventArgs) Handles D1.Click
        piezas(pulsada).Parent = D1
    End Sub

    Private Sub D2_Click(sender As Object, e As EventArgs) Handles D2.Click
        piezas(pulsada).Parent = D2
    End Sub

    Private Sub D3_Click(sender As Object, e As EventArgs) Handles D3.Click
        piezas(pulsada).Parent = D3
    End Sub

    Private Sub D4_Click(sender As Object, e As EventArgs) Handles D4.Click
        piezas(pulsada).Parent = D4
    End Sub

    Private Sub D5_Click(sender As Object, e As EventArgs) Handles D5.Click
        piezas(pulsada).Parent = D5
    End Sub

    Private Sub D6_Click(sender As Object, e As EventArgs) Handles D6.Click
        piezas(pulsada).Parent = D6
    End Sub

    Private Sub D7_Click(sender As Object, e As EventArgs) Handles D7.Click
        piezas(pulsada).Parent = D7
    End Sub

    Private Sub D8_Click(sender As Object, e As EventArgs) Handles D8.Click
        piezas(pulsada).Parent = D8
    End Sub

    Private Sub E1_Click(sender As Object, e As EventArgs) Handles E1.Click
        piezas(pulsada).Parent = E1
    End Sub

    Private Sub E2_Click(sender As Object, e As EventArgs) Handles E2.Click
        piezas(pulsada).Parent = E2
    End Sub

    Private Sub E3_Click(sender As Object, e As EventArgs) Handles E3.Click
        piezas(pulsada).Parent = E3
    End Sub

    Private Sub E4_Click(sender As Object, e As EventArgs) Handles E4.Click
        piezas(pulsada).Parent = E4
    End Sub

    Private Sub E5_Click(sender As Object, e As EventArgs) Handles E5.Click
        piezas(pulsada).Parent = E5
    End Sub

    Private Sub E6_Click(sender As Object, e As EventArgs) Handles E6.Click
        piezas(pulsada).Parent = E6
    End Sub

    Private Sub E7_Click(sender As Object, e As EventArgs) Handles E7.Click
        piezas(pulsada).Parent = E7
    End Sub

    Private Sub E8_Click(sender As Object, e As EventArgs) Handles E8.Click
        piezas(pulsada).Parent = E8
    End Sub

    Private Sub F1_Click(sender As Object, e As EventArgs) Handles F1.Click
        piezas(pulsada).Parent = F1
    End Sub

    Private Sub F2_Click(sender As Object, e As EventArgs) Handles F2.Click
        piezas(pulsada).Parent = F2
    End Sub

    Private Sub F3_Click(sender As Object, e As EventArgs) Handles F3.Click
        piezas(pulsada).Parent = F3
    End Sub

    Private Sub F4_Click(sender As Object, e As EventArgs) Handles F4.Click
        piezas(pulsada).Parent = F4
    End Sub

    Private Sub F5_Click(sender As Object, e As EventArgs) Handles F5.Click
        piezas(pulsada).Parent = F5
    End Sub

    Private Sub F6_Click(sender As Object, e As EventArgs) Handles F6.Click
        piezas(pulsada).Parent = F6
    End Sub

    Private Sub F7_Click(sender As Object, e As EventArgs) Handles F7.Click
        piezas(pulsada).Parent = F7
    End Sub

    Private Sub F8_Click(sender As Object, e As EventArgs) Handles F8.Click
        piezas(pulsada).Parent = F8
    End Sub

    Private Sub G1_Click(sender As Object, e As EventArgs) Handles G1.Click
        piezas(pulsada).Parent = G1
    End Sub

    Private Sub G2_Click(sender As Object, e As EventArgs) Handles G2.Click
        piezas(pulsada).Parent = G2
    End Sub

    Private Sub G3_Click(sender As Object, e As EventArgs) Handles G3.Click
        piezas(pulsada).Parent = G3
    End Sub

    Private Sub G4_Click(sender As Object, e As EventArgs) Handles G4.Click
        piezas(pulsada).Parent = G4
    End Sub

    Private Sub G5_Click(sender As Object, e As EventArgs) Handles G5.Click
        piezas(pulsada).Parent = G5
    End Sub

    Private Sub G6_Click(sender As Object, e As EventArgs) Handles G6.Click
        piezas(pulsada).Parent = G6
    End Sub

    Private Sub G7_Click(sender As Object, e As EventArgs) Handles G7.Click
        piezas(pulsada).Parent = G7
    End Sub

    Private Sub G8_Click(sender As Object, e As EventArgs) Handles G8.Click
        piezas(pulsada).Parent = G8
    End Sub

    Private Sub H1_Click(sender As Object, e As EventArgs) Handles H1.Click
        piezas(pulsada).Parent = H1
    End Sub

    Private Sub H2_Click(sender As Object, e As EventArgs) Handles H2.Click
        piezas(pulsada).Parent = H2
    End Sub

    Private Sub H3_Click(sender As Object, e As EventArgs) Handles H3.Click
        piezas(pulsada).Parent = H3
    End Sub

    Private Sub H4_Click(sender As Object, e As EventArgs) Handles H4.Click
        piezas(pulsada).Parent = H4
    End Sub

    Private Sub H5_Click(sender As Object, e As EventArgs) Handles H5.Click
        piezas(pulsada).Parent = H5
    End Sub

    Private Sub H6_Click(sender As Object, e As EventArgs) Handles H6.Click
        piezas(pulsada).Parent = H6
    End Sub

    Private Sub H7_Click(sender As Object, e As EventArgs) Handles H7.Click
        piezas(pulsada).Parent = H7
    End Sub

    Private Sub H8_Click(sender As Object, e As EventArgs) Handles H8.Click
        piezas(pulsada).Parent = H8
    End Sub

    Private Sub caballoN1_Click(sender As Object, e As EventArgs) Handles caballoN1.Click

    End Sub

    Private Sub arfilN1_Click(sender As Object, e As EventArgs) Handles arfilN1.Click

    End Sub

    Private Sub reinaN_Click(sender As Object, e As EventArgs) Handles reinaN.Click

    End Sub

    Private Sub reyN_Click(sender As Object, e As EventArgs) Handles reyN.Click

    End Sub

    Private Sub arfilN2_Click(sender As Object, e As EventArgs) Handles arfilN2.Click

    End Sub

    Private Sub caballoN2_Click(sender As Object, e As EventArgs) Handles caballoN2.Click

    End Sub

    Private Sub torreN2_Click(sender As Object, e As EventArgs) Handles torreN2.Click

    End Sub

    Private Sub peonN1_Click(sender As Object, e As EventArgs) Handles peonN1.Click

    End Sub

    Private Sub peonN2_Click(sender As Object, e As EventArgs) Handles peonN2.Click

    End Sub

    Private Sub peonN3_Click(sender As Object, e As EventArgs) Handles peonN3.Click

    End Sub

    Private Sub peonN4_Click(sender As Object, e As EventArgs) Handles peonN4.Click

    End Sub

    Private Sub peonN5_Click(sender As Object, e As EventArgs) Handles peonN5.Click

    End Sub

    Private Sub peonN6_Click(sender As Object, e As EventArgs) Handles peonN6.Click

    End Sub

    Private Sub peonN7_Click(sender As Object, e As EventArgs) Handles peonN7.Click

    End Sub

    Private Sub peonN8_Click(sender As Object, e As EventArgs) Handles peonN8.Click

    End Sub

    Private Sub cementerio_Click(sender As Object, e As EventArgs) Handles cementerio.Click
        piezas(pulsada).Parent = cementerio
    End Sub

    Private Sub comenzar_Click(sender As Object, e As EventArgs) Handles comenzar.Click
        PantallaInicio.SendToBack()
        jugador1.Text = IntrJugador1.Text
        jugador2.Text = IntrJugador2.Text

    End Sub

End Class